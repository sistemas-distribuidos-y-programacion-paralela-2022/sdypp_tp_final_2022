package com.casino.casino;

import com.casino.casino.database.entidades.Usuario;
import com.casino.casino.database.servicios.JugadaService;
import com.casino.casino.database.servicios.UsuarioService;
import io.github.cdimascio.dotenv.Dotenv;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;

@RestController
public class GameController {
    Logger logger = LoggerFactory.getLogger(GameController.class);
    // Manejo de la BD
    @Autowired
    private UsuarioService usuarioService;
    @Autowired
    private JugadaService jugadaService;

    @PostConstruct
    public void init(){
        //Cargar variables de entorno
        Dotenv dotenv = Dotenv.configure().load();
        String user_casino= dotenv.get("user_casino","casino" );
        String pass_casino= dotenv.get("pass_casino","password" );
        String cantidad_fichas = dotenv.get("cant_fichas_casino", "900");

        //Seteando Usuario Casino.
        Usuario casino = new Usuario(user_casino, pass_casino, Integer.parseInt(cantidad_fichas));
        this.usuarioService.saveUsuario(casino);
    }

    @GetMapping("/")
    public String Home(@RequestParam(value = "numero" , required = false) String numero,
                       @RequestParam(value = "monto" , required = false) String monto,
                       @RequestParam(value = "mathType" , required = false) String mathType, Model model){


        if(numero!= null){
            switch (mathType){
                case "pluss":
                    if(!numero.isEmpty() && !numero.isBlank()  && !monto.isEmpty() && !monto.isBlank()){
                        int ta = Integer.parseInt(numero);
                        int ta2 = Integer.parseInt(monto);

                        game game = new game();
                        game.startGame();
                        game.setBetAmount(ta2);
                        game.setNumber(ta);

                        if(game.getRouletteNum() == game.getNumber()){
                            int total = (game.getBetAmount() *36) + game.getTotal();
                            model.addAttribute("tree",total);
                            game.setTotal(total);
                            System.out.println(total);
                        }else{
                            int total2 = game.getTotal() - game.getBetAmount();
                            model.addAttribute("four" , total2);
                            game.setTotal(total2);
                            System.out.println(total2);
                        }
                        game game1 = new game();
                        int game3 = game.getRouletteNum();
                        int game4 = game.getNumber();
                        model.addAttribute("one",game3);
                        model.addAttribute("two" , game4);
                    }
                    break;
                default:
                    break;
            }
        }



        return "index.html";

    }

}
