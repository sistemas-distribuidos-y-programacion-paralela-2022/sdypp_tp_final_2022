package com.casino.casino.database.entidades;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sun.istack.NotNull;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
@Entity
@Table(name="jugadas")
public class Jugada {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // le decimos que el id sea autogenerado
    private int idjugada;

    @ManyToOne(fetch = FetchType.LAZY,optional = false)// Lazy porque solo queremos que nos de un dato cuando le indiquemos
    @JoinColumn(name = "idusuario")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Fetch(FetchMode.JOIN)
    private Usuario usuario;
    @NotNull
    private int numero_elegido;
    @NotNull
    private float monto;
    @NotNull
    private char gano;
    @NotNull
    private String estado;

    public int getIdjugada() {
        return idjugada;
    }

    public void setIdjugada(int idjugada) {
        this.idjugada = idjugada;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public int getNumero_elegido() {
        return numero_elegido;
    }

    public void setNumero_elegido(int numero_elegido) {
        this.numero_elegido = numero_elegido;
    }

    public float getMonto() {
        return monto;
    }

    public void setMonto(float monto) {
        this.monto = monto;
    }

    public char getGano() {
        return gano;
    }

    public void setGano(char gano) {
        this.gano = gano;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "Jugada{" +
                "idjugada=" + idjugada +
                ", usuario=" + usuario +
                ", numero_elegido=" + numero_elegido +
                ", monto=" + monto +
                ", gano=" + gano +
                ", estado='" + estado + '\'' +
                '}';
    }
}

