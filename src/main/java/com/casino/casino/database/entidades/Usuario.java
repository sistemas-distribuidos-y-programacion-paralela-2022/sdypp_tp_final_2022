package com.casino.casino.database.entidades;
import com.sun.istack.NotNull;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="usuarios")
public class Usuario {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idusuario;
    @NotNull
    private String username;
    @NotNull
    private String password;
    @NotNull
    private int fichas;
    @OneToMany(mappedBy = "usuario")
    private Set<Jugada> jugadas = new HashSet<>();

    public Usuario(String user_casino, String pass_casino, int cant_fichas) {
        this.username = user_casino;
        this.password = pass_casino;
        this.fichas = cant_fichas;
    }

    public int getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(int idusuario) {
        this.idusuario = idusuario;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getFichas() {
        return fichas;
    }

    public void setFichas(int fichas) {
        this.fichas = fichas;
    }

    public Set<Jugada> getJugadas() {
        return jugadas;
    }

    public void setJugadas(Set<Jugada> jugadas) {
        this.jugadas = jugadas;
    }

    @Override
    public String toString() {
        return "Usuario{" +
                "idusuario=" + idusuario +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", fichas=" + fichas +
                ", jugadas=" + jugadas +
                '}';
    }
}
