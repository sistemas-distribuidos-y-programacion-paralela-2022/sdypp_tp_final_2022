package com.casino.casino.database.repositorios;

import com.casino.casino.database.entidades.Jugada;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JugadaRepository extends JpaRepository<Jugada,Integer>{

}
