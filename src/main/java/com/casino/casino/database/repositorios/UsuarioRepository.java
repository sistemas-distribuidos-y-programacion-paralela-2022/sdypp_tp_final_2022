package com.casino.casino.database.repositorios;

import com.casino.casino.database.entidades.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsuarioRepository extends JpaRepository<Usuario,Integer>{

}
