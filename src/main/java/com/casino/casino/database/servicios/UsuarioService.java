package com.casino.casino.database.servicios;

import com.casino.casino.database.entidades.Usuario;
import com.casino.casino.database.repositorios.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsuarioService {
    @Autowired
    private UsuarioRepository repository;

    public void saveUsuario(Usuario usuario){
        repository.save(usuario);
    }

    public Usuario getUsuario( int idusuario ){
        return repository.findById(idusuario).orElse(null);
    }
}
