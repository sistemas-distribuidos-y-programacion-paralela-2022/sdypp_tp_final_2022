package com.casino.casino.database.servicios;

import com.casino.casino.database.entidades.Jugada;
import com.casino.casino.database.repositorios.JugadaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class JugadaService {
    @Autowired
    private JugadaRepository repository;

    public void saveJugada(Jugada jugada){
        repository.save(jugada);
    }

    public Jugada getJugada(int idjugada){
        return  repository.findById(idjugada).orElse(null);
    }
}
